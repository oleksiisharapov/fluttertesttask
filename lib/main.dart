import 'package:flutter/material.dart';
import 'package:flutter_test_task/BLoC/bloc_provider.dart';
import 'package:flutter_test_task/BLoC/color_bloc.dart';

import 'UI/main_widget.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        bloc: ColorBloc(),
        child: MaterialApp(
          title: 'Flutter Demo',
          theme: ThemeData(
            primarySwatch: Colors.red,
          ),
          home: new MainWidget(),
          debugShowCheckedModeBanner: false,
        ));
  }
}
