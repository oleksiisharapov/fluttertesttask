import 'dart:async';
import 'dart:math';
import 'package:flutter/material.dart';

class ColorBloc {
  Random _intRandom = Random();
  Color _currentColor;
  final _colorController = StreamController<Color>();

  Stream<Color> get getColor => _colorController.stream;

  ColorBloc() {
    generateNewColor();
  }

  void generateNewColor() {
    int redComponent = _intRandom.nextInt(255);
    int greenComponent = _intRandom.nextInt(255);
    int blueComponent = _intRandom.nextInt(255);
    _currentColor =
        Color.fromARGB(0xFF, redComponent, greenComponent, blueComponent);
    _colorController.sink.add(_currentColor);
  }

  void dispose() {
    _colorController.close();
  }
}
