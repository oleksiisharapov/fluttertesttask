import 'package:flutter/material.dart';

class AnimatedText extends StatefulWidget {
  AnimatedTextState createState() => AnimatedTextState();
}

class AnimatedTextState extends State<AnimatedText> {
  bool isTapped = false;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: onTap,
        child: AnimatedDefaultTextStyle(
            style: isTapped
                ? TextStyle(fontSize: 20, color: Colors.red)
                : TextStyle(fontSize: 40, color: Colors.black),
            duration: const Duration(milliseconds: 200),
            child: Text("Hello there")));
  }

  void onTap() {
    setState(() {
      isTapped = !isTapped;
    });
  }
}
