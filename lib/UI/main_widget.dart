import 'package:flutter/material.dart';
import 'package:flutter_test_task/BLoC/bloc_provider.dart';
import 'package:flutter_test_task/BLoC/color_bloc.dart';
import 'package:flutter_test_task/UI/animated_text.dart';

class MainWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var blocProvider = BlocProvider.of<ColorBloc>(context);
    return StreamBuilder<Color>(
        stream: blocProvider.getColor,
        builder: (context, snapshot) {
          var data = snapshot.data;
          return Scaffold(
              appBar: AppBar(title: Text("Flutter test task")),
              body: InkWell(
                  onTap: () {
                    blocProvider.generateNewColor();
                  },
                  child: Container(
                      decoration: BoxDecoration(color: data),
                      child: Center(child: AnimatedText()))));
        });
  }
}
